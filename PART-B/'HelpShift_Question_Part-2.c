/*
//Problem Statement
->Design an efficient program that prints out, in reverse order, every multiple of 7 that is between 1 and 300. Extend the program to other multiples
and number ranges.
*/

#include <stdio.h>

void printRange(int, int, int);
int giveReverseNumber(int);

int main()
{
    int multiple;
    int lowRange, highRange;
    printf("Enter low range : ");
    scanf("%d", &lowRange);
    printf("Enter high range : ");
    scanf("%d", &highRange);
    printf("Enter multiple to reverse from range : ");
    scanf("%d", &multiple);
    printRange(lowRange, highRange, multiple);    
    return 0;
}

//It prints number between lowRange value and highRange value. It prints reverse number which is multiple of multiple variable value.
void printRange(int lowRange, int highRange, int multiple) {
    if(lowRange < highRange) {
        printf("Range from %d to %d with multiple of %d as reverse is : \n", lowRange, highRange, multiple);
        int i;
        for(i=lowRange+1; i<highRange; i++) {
            if(i % multiple == 0) {
                printf("%d\n", giveReverseNumber(i));
            }
            else {
                printf("%d\n",i);
            }
        }
    }
    else {
        printf("Low range should be greater then high range\n");
    }
}

//It takes input as a number and returns reverse of that particular number
int giveReverseNumber(int number) {
    int reverse = 0;
    while(number > 0) {
        reverse = (reverse * 10) + (number % 10);
        number = number / 10;
    }
    return reverse;
}

