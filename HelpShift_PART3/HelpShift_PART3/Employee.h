//
//  Employee.h
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/16/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employee : NSObject

@property (nonatomic, copy) NSString *empName;
@property (nonatomic, assign) int empId;
@property (nonatomic, assign) double salary;

- (instancetype)initWithEmployeeName: (NSString *)empName empId:(int)empId salary:(double)salary;


@end
