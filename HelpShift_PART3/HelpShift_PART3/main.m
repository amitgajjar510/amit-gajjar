//
//  main.m
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/15/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
