//
//  Hotel.m
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/16/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import "Hotel.h"

@implementation Hotel

- (instancetype)initWithHotelName: (NSString *)hotelName ratings:(int)ratings roomRate:(double)roomRate {
    self = [super init];
    if(self) {
        self.hotelName = hotelName;
        self.ratings = ratings;
        self.roomRate = roomRate;
    }
    return  self;
}

#pragma NSCoding
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    NSString *hotelName = (NSString *)[aDecoder decodeObjectForKey:@"hotelName"];
    int ratings = (int)[aDecoder decodeIntegerForKey:@"ratings"];
    double roomRate = (double)[aDecoder decodeDoubleForKey:@"roomRate"];
    if(hotelName == nil) {
        return nil;
    }
    return [self initWithHotelName:hotelName ratings:ratings roomRate:roomRate];
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.hotelName forKey:@"hotelName"];
    [aCoder encodeInteger:self.ratings forKey:@"ratings"];
    [aCoder encodeDouble:self.roomRate forKey:@"roomRate"];
}

@end
