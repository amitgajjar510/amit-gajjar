//
//  KVStore.h
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/15/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//
//****************************************************************************************************
//*****************************Introduction of KVStore************************************************
//****************************************************************************************************
// KVStore is a Key-Value store for storing any objects which is serialized on to the Persistant store.
// Here Mutable dictionary is used to store key and Values. Keys should be NSString only and
// Values can be any object which is serialized i.e. implemented NSCoding protocol.
// Here I have used File to store Mutable Dictionary as a Persistant store.
// Some of the examples available in ViewController.m file.
//****************************************************************************************************
//****************************************************************************************************

#import <Foundation/Foundation.h>

@interface KVStore : NSObject

//Method to fetch the value stored by Key
- (id)getValueForKey:(NSString *)key;

//Method to set the value on Key
- (BOOL)setValue:(id)value forKey:(NSString *)key;

//Store values on Persistant store.
//MUST call this method after setting or updating any value.
- (BOOL)synchronizeData;

@end
