//
//  Hotel.h
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/16/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hotel : NSObject <NSCoding>

@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, assign) int ratings;
@property (nonatomic, assign) double roomRate;

- (instancetype)initWithHotelName: (NSString *)hotelName ratings:(int)ratings roomRate:(double)roomRate;

@end
