//
//  ViewController.m
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/15/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import "ViewController.h"
#import "KVStore.h"
#import "Hotel.h"
#import "Employee.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Initialized KVStore
    KVStore *store = [[KVStore alloc] init];
    
    //Storing Integer Value
    [store setValue:[NSNumber numberWithInt:10] forKey:@"intValue"];
    //Storing Float Value
    [store setValue:[NSNumber numberWithFloat:20.30] forKey:@"floatValue"];
    
    //Instantiating Serialized object and storing to persistant store i.e. File
    Hotel *h = [[Hotel alloc] initWithHotelName:@"Marriott" ratings:4 roomRate:10000.0];
    [store setValue:h forKey:@"hotelObj"];

    //Instatinating Normal object Without serialization and it gives error.
    //Error : "Object is not serialized. Make it serialized to store in KVStore."
    Employee *e = [[Employee alloc] initWithEmployeeName:@"ABC" empId:123 salary:25000.0];
    if([store setValue:e forKey:@"empObj"]) {
        NSLog(@"Successfully stored Employee object");
    }
    else {
        NSLog(@"Failed to store Employee object");
    }
    
    //Store all the values into the persistant store i.e. file.
    [store synchronizeData];
    
    //Getting value of the object from persistant store.
    Hotel *ret = [store getValueForKey:@"hotelObj"];
    NSLog(@"Get Value : %@",ret);
    NSLog(@"Name : %@\n Ratings : %d\n Room Rate : %f", ret.hotelName, ret.ratings, ret.roomRate);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
