//
//  KVStore.m
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/15/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import "KVStore.h"


@implementation KVStore

static NSString *kvStoreDictFileName = @"kvStoreDict.out";
NSMutableDictionary *dataDictionary = nil;

//Get Dictionary which is already stores on the File. If not found create new dictionary.
- (instancetype)init {
    self = [super init];
    if(self != nil) {
        dataDictionary = [self fetchDictionaryFromFile];
        NSLog(@"Before Dictionary : %@",dataDictionary);
        if(dataDictionary == nil) {
            dataDictionary = [[NSMutableDictionary alloc] init];
        }
        NSLog(@"After Dictionary : %@",dataDictionary);
    }
    return self;
}

#pragma mark - Private Methods
//Method to get Document directory path
- (NSString *)documentDirectoryPath {
    NSString *docDirPath = nil;
    NSArray *arrayOfPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if([arrayOfPaths count] > 0) {
        docDirPath = [arrayOfPaths objectAtIndex:0];
    }
    return docDirPath;
}

//Method to get path of Stored Dictionary on file
- (NSString *)getKVStoreDictFilePath {
    return [[self documentDirectoryPath] stringByAppendingString:kvStoreDictFileName];
}

//Method to fetch dictionary from File
- (NSMutableDictionary *)fetchDictionaryFromFile {
    NSMutableDictionary *dictionaryFromFile = nil;
    NSString *kvStoreDictFilePath = [self getKVStoreDictFilePath];
    
    NSMutableData *dataFromFile = [[NSMutableData alloc] initWithContentsOfFile:kvStoreDictFilePath];
    if(dataFromFile != nil) {
        dictionaryFromFile = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:dataFromFile];
    }
    return dictionaryFromFile;
}

//Method to save the Dictionary on Persistant store i.e. in File
- (BOOL)writeDictionaryToFile:(NSMutableDictionary *)dictionary {
    BOOL success = NO;
    NSString *kvStoreDictFilePath = [self getKVStoreDictFilePath];
    if([NSKeyedArchiver archiveRootObject:dictionary toFile:kvStoreDictFilePath]) {
        success = YES;
    }
    return success;
}

#pragma mark - Public Methods
//Method to fetch the value stored by Key
- (id)getValueForKey:(NSString *)key {
    return [dataDictionary objectForKey:key];
}

//Method to set the value on Key
-(BOOL)setValue:(id)value forKey:(NSString *)key {
    //CHECK FOR NSCoding in implemented for Custom Classes.
    if([value conformsToProtocol:@protocol(NSCoding)]) {
        [dataDictionary setValue:value forKey:key];
    }
    else {
        NSLog(@"Object is not serialized. Make it serialized to store in KVStore.\n");
        return NO;
    }    
    return YES;
}

//Store values on Persistant store.
//MUST call this method after setting or updating any value.
- (BOOL)synchronizeData {
    return [self writeDictionaryToFile:dataDictionary];
}

@end
