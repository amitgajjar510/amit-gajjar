//
//  Employee.m
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/16/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import "Employee.h"

@implementation Employee

- (instancetype)initWithEmployeeName: (NSString *)empName empId:(int)empId salary:(double)salary {
    self = [super init];
    if(self) {
        self.empName = empName;
        self.empId = empId;
        self.salary = salary;
    }
    return  self;
}


@end
