//
//  AppDelegate.h
//  HelpShift_PART3
//
//  Created by Amit Gajjar on 6/15/16.
//  Copyright © 2016 AmitGajjar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

